const {Client} = require("@notionhq/client");


module.exports = function(RED){
    function NotionCredentialsNode(n){
        RED.nodes.createNode(this, n);
        this.client = new Client({auth: this.credentials.token});
    }

    RED.nodes.registerType("notion-credentials", NotionCredentialsNode, {
        credentials: {
            token: {type: "password"}
        }
    });

    function NotionDbQueryNode(config) {
        RED.nodes.createNode(this, config);
        const node = this;
        this.client = RED.nodes.getNode(config.account).client;
        node.on('input', function(msg, send, done) {
            if(this.client){
                const filter = RED.util.evaluateNodeProperty(config.filter, config.filterType, config, msg);
                const sorts = RED.util.evaluateNodeProperty(config.sort, config.sortType, config, msg);

                const next_cursor = msg?.payload?.next_cursor;
                
                this.client.databases.query({
                    database_id: RED.util.evaluateNodeProperty(config.database, config.databaseType, config, msg),
                    filter: Object.keys(filter).length === 0 ? undefined : filter,
                    sorts: sorts.length > 0 ? sorts : undefined,
                    start_cursor: next_cursor ? next_cursor : undefined
                })
                    .then(dbitems => {
                        msg.config = config;
                        msg.payload = dbitems;
                        send(msg);
                        done();
                    })
                    .catch(e => done(e))
            }
        });
    }

    RED.nodes.registerType('notion-db-query', NotionDbQueryNode)

    function NotionDbNode(config) {
        RED.nodes.createNode(this, config);
        const node = this;
        this.client = RED.nodes.getNode(config.account).client;
        node.on('input', function(msg, send, done) {
            if(this.client){
                this.client.databases.retrieve({
                    database_id: RED.util.evaluateNodeProperty(config.database, config.databaseType, config, msg),
                })
                    .then(dbobj => {
                        msg.payload = dbobj;
                        send(msg);
                        done();
                    })
                    .catch(e => done(e))
            }
        });
    }

    RED.nodes.registerType('notion-db', NotionDbNode)

    function NotionPageBlocksNode(config) {
        RED.nodes.createNode(this, config);
        const node = this;
        this.client = RED.nodes.getNode(config.account).client;
        node.on('input', function(msg, send, done) {
            if(this.client){
                const pageid = RED.util.evaluateNodeProperty(config.pageid, config.pageidType, config, msg);
                this.client.blocks.children.list({
                    block_id: pageid
                })
                    .then(pageblocks => {
                        msg.payload = pageblocks;
                        send(msg);
                        done();
                    })
                    .catch(e => done(e))
            }
        });
    }

    RED.nodes.registerType('notion-page-blocks', NotionPageBlocksNode)

    function NotionBlocksHTMLParserNode(config) {

    }
}